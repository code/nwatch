Nwatch
======

Slightly friendlier version of the suggested script in the manual page for
[`ndiff(1)`](http://linux.die.net/man/1/ndiff) from the
[Nmap](http://nmap.org/) suite, implemented in Bash.

Usage:

    $ nwatch HOSTLIST CACHEDIR

Example with root privileges:

    # nwatch /etc/nwatch.mynet /var/cache/nwatch/mynet

Prints the filtered results of an `ndiff(1)` call against the last
known scan to stdout; intended to be called from `cron(8)`:

    0 0 * * 0  nwatch /etc/nwatch.mynet /var/cache/nwatch/mynet

I recommend you use
[Mail::Run::Crypt](https://metacpan.org/pod/Mail::Run::Crypt), so you don't
leak your network information in plain text in your email.

Please also consider whether your scan actually requires root privileges, and
could not instead be run by a dedicated user with appropriately limited
privileges.

License
-------

Copyright (c) [Tom Ryder](https://sanctum.geek.nz/). Distributed under
[GPLv2](https://www.gnu.org/licenses/gpl-2.0.html), same as Nmap itself; see
`LICENSE`.

Nmap is a registered trademark of Insecure.Com LLC, and this project is not
affiliated with it.
